import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, LoadingController, Loading } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { Signup } from '../pages/signup/signup'
import { API } from '../providers/api/api'

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('mainContent') nav: NavController // now we have a nav controller available as this.nav
  
  // rootPage:any = TabsControllerPage
  // rootPage:any = Signup;
  rootPage:any;
  private loader: Loading;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
              private api: API, private loadingCtrl: LoadingController) {
    
    platform.ready().then(() => {
      statusBar.styleDefault();
      // splashScreen.hide();
    });

    this.showLoader();
    this.api.signIn()
     .then(isLoggedIn => {
       this.rootPage = isLoggedIn ? TabsControllerPage : Signup;
       splashScreen.hide();
       this.loader.dismiss();
    }) 
  }

  showLoader () {
    this.loader = this.loadingCtrl.create({
      content: 'Авторизуемся....',
    })
    this.loader.present();
  }
  
}
