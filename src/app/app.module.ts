import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { List } from '../pages/list/list';
import { ViewOnMap } from '../pages/list/map.component'
import { ViewList } from '../pages/list/list.component'
import { NavigationPage } from '../pages/navigation/navigation';
import { OrdersListPage } from '../pages/orders/orders';
import { SettingsPage } from '../pages/settings/settings';
import { TabsControllerPage } from '../pages/tabs-controller/tabs-controller';
import { Signup } from '../pages/signup/signup';
import { SMSConfirm } from '../pages/sms/sms';
import { BillingPage } from '../pages/billing/billing';
import { Welcome } from '../pages/welcome/welcome';
import { DetailsPage } from '../pages/details/details';
import { TimePage } from '../pages/time/time';
import { SuggestPage } from '../pages/suggest/suggest';
import { BookingPage } from '../pages/booking/booking';
import { DonePage } from '../pages/done/done';

import { TextMaskModule } from 'angular2-text-mask';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Device } from '@ionic-native/device';
import { Geolocation } from '@ionic-native/geolocation';
import { LaunchNavigator } from '@ionic-native/launch-navigator';

import { API } from '../providers/api/api';
import { ConnectivityService } from '../providers/connectivity-service/connectivity-service';
import { GoogleMapsProvider } from '../providers/google-maps/google-maps';

import { Ionic2RatingModule } from 'ionic2-rating';
import { NotificationsProvider } from '../providers/notifications/notifications';

@NgModule({
  declarations: [
    MyApp,
    List,
    NavigationPage,
    OrdersListPage,
    SettingsPage,
    TabsControllerPage,
    Signup,
    SMSConfirm,
    BillingPage,
    Welcome,
    DetailsPage,
    TimePage,
    SuggestPage,
    BookingPage,
    DonePage,
    ViewOnMap,
    ViewList
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    TextMaskModule,
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    List,
    OrdersListPage,
    SettingsPage,
    TabsControllerPage,
    Signup,
    SMSConfirm,
    BillingPage,
    Welcome,
    DetailsPage,
    TimePage,
    SuggestPage,
    BookingPage,
    DonePage,
    NavigationPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LaunchNavigator,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    API,
    Geolocation,
    Device,
    ConnectivityService,
    GoogleMapsProvider,
    NotificationsProvider
  ]
})
export class AppModule {}