// TODO: create a specific user-data provider and correct environment settings
// https://forum.ionicframework.com/t/ionic-2-environment-variables-as-simple-as-possible/80918
// https://github.com/ionic-team/ionic-conference-app/blob/master/src/providers/user-data.ts
export class AppSettings {
  public static API_ENDPOINT_DEV: string = 'http://localhost:8100/api'
  public static API_ENDPOINT_PROD: string = 'http://www.grandquestion.ru';
  public static DEV_UUID: string[10] = '0000000001';
  public static ENV: string = 'any';
}