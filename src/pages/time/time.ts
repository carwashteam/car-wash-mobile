import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { CarWash } from '../list/carWash.interface';
import { API } from '../../providers/api/api';

import { BookingPage } from '../booking/booking';
import { SuggestPage } from '../suggest/suggest';

@Component({
  selector: 'page-time',
  templateUrl: 'time.html'
})
export class TimePage {
  bookingTime: any = '00:00';
  private poi: CarWash;
  private formData: string[];
  serviceNames: string[];

  constructor(private navCtrl: NavController, private navParams: NavParams, private api: API) {
    this.poi = navParams.get('poi');
    this.formData = navParams.get('formData');
    this.serviceNames = navParams.get('serviceNames');
  }

  async checkBooking () {
    // TODO: Convert bookingTime to timestamp
    console.info('Before shipping data to API: ', this.poi.id, ' | ' , this.formData, ' | ' , this.bookingTime);
    const newOrder = await this.api.createOrder(this.poi.id, this.formData, this.convertTime(this.bookingTime));
    if (newOrder.success) {
      this.goBooking(newOrder.data, this.serviceNames, this.poi);
    } else if (newOrder.freetimes) {
      this.goSuggestTime(this.poi, newOrder.freetimes, this.formData, this.serviceNames);
    }
  }

  goBooking (newOrder, orderedServicesNames, poi) {
    this.navCtrl.push(BookingPage, { newOrder, orderedServicesNames, poi });
  }

  goSuggestTime (poi, availableTimes, orderedServicesIds, orderedServicesNames) {
    this.navCtrl.push(SuggestPage, { poi, availableTimes, orderedServicesIds, orderedServicesNames })
  }

  convertTime(time: string): string {
    const currentDate = new Date();
    const timeParts = time.split(':');
    currentDate.setHours(+timeParts[0]);
    currentDate.setMinutes(+timeParts[1]);
    return currentDate.getTime().toString();
  }
  
}
