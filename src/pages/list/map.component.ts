import { Component, Input, Output, ViewChild, ElementRef, OnInit, OnDestroy, NgZone, EventEmitter } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';
// import { ConnectivityService } from '../../providers/connectivity-service/connectivity-service'
import { GoogleMapsProvider } from '../../providers/google-maps/google-maps'
import { Geolocation } from '@ionic-native/geolocation';
import { CarWash } from './carWash.interface'

@Component({
  selector: 'view-map',
  template: `<div #map id="map">
            {{text}}
            </div>`
})
export class ViewOnMap implements OnInit, OnDestroy {
  @Input() 
  set poi (poi: Array<CarWash>) {
    this._poi = poi || [];
    if(this._poi.length > 0 && this.map) this.showMarkers()
  }

  @Output() onDetails = new EventEmitter<string>();

  @ViewChild('map') mapElement: ElementRef;
  
  private _poi: Array<CarWash> = [];
  // private _markers: Array<google.maps.Marker> = [];
  private _markers: any[];
  private map: google.maps.Map;
  private infoWindow: google.maps.InfoWindow;
  private loader: Loading;

  constructor(
    private googleMapsProvider: GoogleMapsProvider,
    private geolocation: Geolocation, private zone: NgZone,
    private loadingCtrl: LoadingController
  ) {
    (window as any).angularComponent = {
      showPOIDetails: this.showPOIDetails,
      zone: zone,
      onDetails: this.onDetails
    }
  }
  // *** lifecycle hooks ***
  ngOnInit () {
    this.initMap(this.mapElement);
  }

  ngOnDestroy () {
    google.maps.event.clearListeners(this.map, 'idle');
    this.map = null;
    this.infoWindow = null;
    this._markers = null;
    this._poi = null;
    (window as any).angularComponent = null;
  }
  
  // *** public members ***

  showPOIDetails (id: string) {
    this.zone.run(() => {
      console.info('Info Window clicked!', id);
      this.onDetails.emit(id);
    })
  }

  showMarkers () {
    this.infoWindow = this.googleMapsProvider.createInfoWindow();
    this._markers = this._poi.map((poi) => {
      const mapPoint: google.maps.LatLng = new google.maps.LatLng(poi.latitude, poi.longitude);
      const marker = this.googleMapsProvider.createMarker(mapPoint);

      const contentTemplate =  `
        <strong>${poi.name}</strong>
        <p>Рейтинг: ${poi.rating} из 5</p>
        <button onclick="window.angularComponent.showPOIDetails('${poi.id}')">Подробнее</button>
      `;
      
      this.googleMapsProvider.addInfoWindowToMarker(this.infoWindow, this.map, marker, contentTemplate);
      return marker;
    })

    this.googleMapsProvider.showMarkers(this.map, this._markers);
  }

  async initMap (element: ElementRef) {
    this.showLoader();
    const position = await this.geolocation.getCurrentPosition();
    console.log('Current location: ', position.coords.latitude, position.coords.longitude);
    let homePoint = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); // 55.751244, 37.618423 - Moscow center
    
    this.map = this.googleMapsProvider.initMap(element, homePoint)
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      console.log("%cMAP LOADED", "color:red;font-weight:700;");
      this.loader.dismiss();
    });
    this.googleMapsProvider.addCarMarkerOnMap(homePoint, this.map, '<b>Тачка твоя тут!</b>', true);
    if(this._poi.length > 0) this.showMarkers()
  }

  showLoader () {
    this.loader = this.loadingCtrl.create({
      content: 'Загружаем карту....',
    })
    this.loader.present();
  }

}