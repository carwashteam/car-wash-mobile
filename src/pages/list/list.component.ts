import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CarWash } from './carWash.interface'


@Component({
  selector: 'view-list',
  templateUrl: 'list.component.html'
})
export class ViewList {
  @Input() 
  set poi (poi: Array<CarWash>) {
    this._poi = poi;
    // if(poi.length > 0 && this.map) this.showMarkers()
  }
  @Output() onDetails = new EventEmitter<string>();

  public _poi: Array<CarWash> = [];

  constructor() {
  }

  showPOIDetails (poi: any) {
    // console.info('POI clicked!', poi);
    this.onDetails.emit(poi.id);
  }

}