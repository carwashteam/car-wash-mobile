import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NotificationsProvider } from '../../providers/notifications/notifications';

import { API } from '../../providers/api/api';

import { DetailsPage } from '../details/details';

@Component({
  selector: 'list-page',
  templateUrl: 'list.html'
})
export class List {

  public poi: Array<Object> = [];
  private normalizedPOI: Object = {};
  public viewMode: string = '';
  // FIXME: Not DRY code. We should use some kind of service for loader.show, loader hide...

  constructor(
    public navCtrl: NavController,
    private api: API,
    private notifications: NotificationsProvider 
  ) {
    this.viewMode = 'map'; // default segment
  }
  
  // *** lifecycle hooks ***
  ionViewDidLoad () {
    this.getList();
  }
  // *** public members ***
  segmentChanged ({_value}) {
    console.log('Segment changed: ', _value);
  }

  showPOIDetails (id: string) {
    // console.info('<List /> wants to show Car Wash POI id: ', id);
    this.navCtrl.push(DetailsPage, { poi: this.normalizedPOI[id] });
  }

  getList () {
    this.notifications.showLoader('Загружаем данные....');
    return this.api.getList()
      .subscribe(({data}) => {
        this.poi = data;
        this.normalizedPOI = this.normalize(data);
        this.notifications.dismissLoader();
      })
  }

  normalize (data: Array<any>): Object {
    return data.reduce((map, obj) => {
        map[obj.id] = obj;
        return map;
    }, {});
  }
  
}
