
export interface CarWash {
  latitude: number,
  longitude: number,
  name: string,
  rating: number,
  id: string,
  extra: any[],
  additional: any[],
  standard: any[],
  maxPrice: string,
  minPrice: string,
  travelTime: string,
  bookingTime?: string
}
