import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NavigationPage } from '../navigation/navigation';
// import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { CarWash } from '../list/carWash.interface';


@Component({
  selector: 'booking-page',
  templateUrl: 'booking.html'
})
export class BookingPage {
  private poi: CarWash;
  newOrder: Object = {
    code: '',
    box: 0,
    start_time: ''
  };
  ordered: string[] = [];

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    // private launchNavigator: LaunchNavigator
  ) {
    this.ordered = navParams.get('orderedServicesNames');
    this.newOrder = navParams.get('newOrder');
    this.poi = navParams.get('poi');
    //  = this.getServiceNames(formData, );
    
  }

// TODO: have to show booking details data received from API
  goToNavigation () {
    console.info('%cLaunching navigation %o', 'color:blue;font-weight:700;', this.poi.latitude, this.poi.longitude);
    this.navCtrl.push(NavigationPage, {poi: this.poi});
    // according to envirement -emul, web/device - call a LaunchNavigator or navigating to NavigationPage
    // const options: LaunchNavigatorOptions = {
    //   start: 'London, ON',
    // };
    // this.launchNavigator.navigate([this.poi.latitude, this.poi.longitude])
    //   .then(
    //     success => console.info('%сLaunched navigator', 'color:green;'),
    //     error => console.info('%cError launching navigator %o', 'color:red;', error)
    //   );
  }
  
}
