import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { API } from '../../providers/api/api';
import { BookingPage } from '../booking/booking';
import { NotificationsProvider } from '../../providers/notifications/notifications';

import { CarWash } from '../list/carWash.interface';

@Component({
  selector: 'suggest-page',
  templateUrl: 'suggest.html'
})
export class SuggestPage {
  private poi: CarWash;
  public availableTimes: string[3];
  private orderedServicesIds: number[];
  private orderedServicesNames: string[];

  constructor(
    public navCtrl: NavController,
    private navParams: NavParams,
    private api: API,
    private notifications: NotificationsProvider
  ) {
    this.poi = navParams.get('poi');
    this.availableTimes = navParams.get('availableTimes');
    this.orderedServicesIds = navParams.get('orderedServicesIds');
    this.orderedServicesNames = navParams.get('orderedServicesNames');
  }

  toTimeString (timestamp: string): string {
    const date = new Date((+timestamp) * 1000);
    const hours = date.getHours();
    const minutes = date.getMinutes();
    return `${hours}:${minutes}`;
  }

  async tryBooking (timestamp) {
    const newOrder = await this.createOrder(this.poi, this.orderedServicesIds, timestamp)
    if (newOrder.success) {
      this.goBooking(newOrder.data, this.orderedServicesNames, this.poi);
    } else if (newOrder.freetimes) {
      // this.goSuggestTime(params, newOrder.freetimes, formData);
      this.availableTimes = newOrder.freetimes;
      this.notifications.presentToast('Извините, но это время уже занято, выберите другое...');
    }
  }

  async createOrder (poi: CarWash, serviceIds, time) {
    return this.api.createOrder(poi.id, serviceIds, time)
  }

  goBooking (newOrder, orderedServicesNames, poi) {
    debugger;
    this.navCtrl.push(BookingPage, { newOrder, orderedServicesNames, poi });
  }
  
}
