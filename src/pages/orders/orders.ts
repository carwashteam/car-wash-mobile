import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NotificationsProvider } from '../../providers/notifications/notifications';

import { API } from '../../providers/api/api';

@Component({
  selector: 'orders-page',
  templateUrl: 'orders.html'
})
export class OrdersListPage {
  public orders: Array<Object> = [];

  constructor(
    public navCtrl: NavController,
    private api: API,
    private notifications: NotificationsProvider 
  ) {
  }
  // *** lifecycle hooks ***
  ionViewWillEnter() {
    this.getOrdersList();
  }
  // *** private members ***
  private getOrdersList () {
    this.notifications.showLoader('Загружаем данные....');
    return this.api.getOrders()
      .subscribe(({data}) => {
        this.orders = data;
        this.notifications.dismissLoader();
      })
  }
  // *** public members ***

}
