import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BillingPage } from '../billing/billing';
import { API } from '../../providers/api/api';

@Component({
  selector: 'sms-page',
  templateUrl: 'sms.html'
})
export class SMSConfirm {
  private mask: Object;
  pin: string[4] = '';

  constructor(public navCtrl: NavController, private api: API) {
    this.mask = {
      PIN: [/\d/, /\d/, /\d/, /\d/]
    };
  }

  async confirmSMS () {
    // TODO: If SMS is not confirmed - show toast
    const pin = this.pin.replace(/\D+/g, '').substring(0, 4); // fuck this bug...
    const success = await this.api.confirmSMS(pin);
    console.info('%cSMS code: %o', 'color:green; font-weight:700;', pin);
    console.info('%cSMS confirmed: %o', 'color:green; font-weight:700;', success);
    // TODO: We have a dedicated Page 'Регистрация прошла успешно' page8 - use it!
    this.navCtrl.push(BillingPage);
  }
  
}
