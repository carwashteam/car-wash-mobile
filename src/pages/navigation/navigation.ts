import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NotificationsProvider } from '../../providers/notifications/notifications';
import { GoogleMapsProvider } from '../../providers/google-maps/google-maps'
import { Geolocation } from '@ionic-native/geolocation';
import { CarWash } from '../list/carWash.interface';



@Component({
  selector: 'navigation-page',
  templateUrl: 'navigation.html',
})
export class NavigationPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('directionsPanel') directionsPanel: ElementRef;
  
  private map: google.maps.Map;
  private POI: CarWash;

  constructor(
    public navCtrl: NavController, private navParams: NavParams,
    private googleMapsProvider: GoogleMapsProvider,
    private geolocation: Geolocation,
    private notifications: NotificationsProvider,
  ) {
    this.POI = navParams.get('poi');
  }

  ionViewDidLoad() {
    this.initMapAndNavigate(this.mapElement);
  }

  async initMapAndNavigate (element: ElementRef) {
    this.notifications.showLoader('Строим маршрут...');
    const position = await this.geolocation.getCurrentPosition();
    const origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude); // 55.751244, 37.618423 - Moscow center
    const destination = new google.maps.LatLng(this.POI.latitude , this.POI.longitude);
    this.map = this.googleMapsProvider.initMap(element, origin)
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      console.log("%cMAP LOADED, location: %o", "color:red;font-weight:700;", position.coords.latitude, position.coords.longitude);
      this.notifications.dismissLoader();
    });
    this.googleMapsProvider.addCarMarkerOnMap(origin, this.map, '<b>Вот твоя тачка, чувак!</b>', true);
    this.googleMapsProvider.startNavigating(origin, destination, this.map, this.directionsPanel);
  }

  
  
}
