import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PhoneValidator } from './validators/phone.validator';
import { API } from '../../providers/api/api';

import { SMSConfirm } from '../sms/sms';

@Component({
  selector: 'signup-page',
  templateUrl: 'signup.html'
})
export class Signup {
  private signupForm: FormGroup;
  private mask: Object;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    private api: API
  ) {
    this.mask = {
      phoneNumber: ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /[0-9]/]
    };
    this.signupForm = this.formBuilder.group({
      phone: ['', PhoneValidator.isValid]
    });
  }

  private cutThePhone (numbers: string) {
    return numbers.length > 10 ? numbers.substring(0, 10) : numbers; // polyfill for extra char side-effect
  }
  
  async next () {
    let phone = this.cutThePhone(this.signupForm.value.phone.replace(/\D+/g, ''))
    const success = await this.api.signUp(phone)
    
    console.info('%cPhone: %o', 'color:deeppink;', phone);
    console.warn('%cSignUp success: %o', 'color:deeppink;font-weight:700;', (success ? 'yep':'nope'));
    
    if(success) this.navCtrl.push(SMSConfirm);
  }
  
}
