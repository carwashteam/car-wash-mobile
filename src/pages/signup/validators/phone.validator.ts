import { FormControl } from '@angular/forms';
 
export class PhoneValidator {
 
  static isValid (control: FormControl): any {
    const strippedValue = control.value.replace(/\D+/g, '');
    if(isNaN(strippedValue)){
      return {
        "Телефон должен содержать только цифры": true
      };
    }

    if(strippedValue.toString().length < 10){
      return {
        "Телефон должен содержать 10 символов": true
      };
    }

    return null;
  }
}