import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { OrdersListPage } from '../orders/orders';
import { List } from '../list/list';
import { SettingsPage } from '../settings/settings';
// import { BillingPage } from '../billing/billing';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = List;
  tab2Root: any = OrdersListPage;
  tab3Root: any = SettingsPage;
  tab4Root: any = SettingsPage;
  tab5Root: any = SettingsPage;

  constructor(public navCtrl: NavController) {
  }

  goToOrders(params){
    if (!params) params = {};
    this.navCtrl.push(OrdersListPage);
  }
}
