import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';

import { TimePage } from '../time/time';
import { BookingPage } from '../booking/booking';
import { SuggestPage } from '../suggest/suggest';

import { GoogleMapsProvider } from '../../providers/google-maps/google-maps'
import { NotificationsProvider } from '../../providers/notifications/notifications';
import { Geolocation } from '@ionic-native/geolocation';

import { API } from '../../providers/api/api';

import { CarWash } from '../list/carWash.interface';

import _filter from 'lodash/filter';
import _get from 'lodash/get';
import _toNumber from 'lodash/toNumber';

const DEFAULT_TRAVEL_TIME = 'расчитываем...';

@Component({
  selector: 'details-page',
  templateUrl: 'details.html'
})
export class DetailsPage {
  carWash: CarWash; // FIXME: Add CarWash interface compliance later
  private detailsForm: FormGroup;

  constructor (
    public navCtrl: NavController,
    private navParams: NavParams,
    private googleMapsProvider: GoogleMapsProvider,
    private geolocation: Geolocation,
    private zone: NgZone,
    private formBuilder: FormBuilder,
    private notifications: NotificationsProvider,
    private api: API
  ) {
    this.carWash = navParams.get('poi');
    this.carWash.travelTime = DEFAULT_TRAVEL_TIME;
    const standardServices = this.compileFields(this.carWash.standard);
    const addServices = this.compileFields(this.carWash.additional);
    console.info('%cstandardServices names: %o', 'color:orange;', standardServices);
    console.info('%caddServices names: %o', 'color:orange;', addServices);

    this.detailsForm = this.formBuilder.group(
      {...standardServices, ...addServices}
    );
  }
  // hooks
  ionViewDidLoad() {
    // this.calculateTravelTime(this.carWash);
    // this.notifications.showError('Testing error toasts');
  }
  // form routine
  submit () {
    // console.info('%conSubmit detailsForm values: %o', 'color:orange;', this.detailsForm.value);
    // const temp = this.decompileFields(this.detailsForm.value);
    // console.info('%cready for POST values: %o', 'color:orange;', temp);
  }
  compileFields (servicesArray: any[], namePrefix?: string) {
    return servicesArray.reduce(
      (servicesMap, service) => {
        servicesMap[`${namePrefix || ''}${service.id}`] = [false];
        return servicesMap;
      }, {});
  }

  decompileFields (fieldsMap: any): number[] {
    let fields: number[] = [];
    for (let field in fieldsMap) {
      if(fieldsMap[field]) fields.push(_toNumber(field));
    }
    return fields;
  }

  cutTheCrap (crap: string = ''): number {
    let hours = 0, minutes = 0;

    crap = crap.toString();
    let hourIndex = crap.indexOf('час.'); // It can be either час or ч
    if (hourIndex === -1) hourIndex = crap.indexOf('ч.');
    let minutesIndex = crap.indexOf('мин.');
    if (minutesIndex === -1) minutesIndex = crap.indexOf('м.');

    if (hourIndex !== -1 && minutesIndex !== -1) {
      hours = + crap.slice(0, hourIndex-1);
      minutes = + crap.slice(hourIndex+2, minutesIndex-1);
    } else if (hourIndex === -1 ) {
      hours = 0;
      minutes = + crap.slice(0, minutesIndex-1);
    } else if (minutesIndex === -1) {
      hours = + crap.slice(0, hourIndex-1);;
      minutes = 0;
    }
    console.log('the shit out: ', hours, ' часов ', minutes, ' минут');
    return hours*60 + minutes*1;
  }

  async calculateTravelTime (poi: CarWash) {
    this.notifications.showLoader('Выясняем время в пути...');
    const position = await this.geolocation.getCurrentPosition();
    const origin = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    const destination = new google.maps.LatLng(poi.latitude , poi.longitude);
    this.googleMapsProvider.calculateDrivingTime(origin, [destination], this.setTravelTime);
  }

  private setTravelTime = (distanceMatrixResponce, status) => {
    this.zone.run(() => {
      if (status == google.maps.DistanceMatrixStatus.OK) {
        console.info('Время в пути: ', distanceMatrixResponce.rows[0].elements[0].duration_in_traffic.text);
        this.carWash['travelTime'] = distanceMatrixResponce.rows[0].elements[0].duration_in_traffic.text;
      } else {
        console.warn('Failed calculate distance: ', status)
      }
      this.notifications.dismissLoader();
    })
  }
  
  goToTime (params) {
    if (!params) params = {};
    const formData = this.decompileFields(this.detailsForm.value);
    if (params.travelTime) params.travelTime = this.cutTheCrap(params.travelTime);
    this.navCtrl.push(TimePage, {poi: params, formData, serviceNames: this.getServiceNames(formData, params)});
  }

  async goToBookingDetails (params) {
    if (!params) params = {};
    params.travelTime = (params.travelTime !== DEFAULT_TRAVEL_TIME) ? this.cutTheCrap(params.travelTime) : 0;
    params.bookingTime = Date.now() + (1000 * (+params.travelTime)); // booking considering travel time
    const formData = this.decompileFields(this.detailsForm.value);
    const serviceNames = this.getServiceNames(formData, params);
    const newOrder = await this.createOrder(params, formData);
    if (newOrder.success) {
      this.goBooking(newOrder.data, serviceNames, params);
    } else if (newOrder.freetimes) {
      this.goSuggestTime(params, newOrder.freetimes, formData, serviceNames);
    }
  }

  async createOrder (poi, formData) {
    return this.api.createOrder(poi.id, formData, poi.bookingTime)
  }

  getServiceNames (ids: number[], poi: CarWash): string[] {
    const services = poi.standard.concat(poi.additional);
    return ids.map(id => _get(_filter(services, service => service.id == +id)[0], 'name'));
  }

  goBooking (newOrder, orderedServicesNames, poi) {
    this.navCtrl.push(BookingPage, { newOrder, orderedServicesNames, poi });
  }

  goSuggestTime (poi, availableTimes, orderedServicesIds, orderedServicesNames) {
    this.navCtrl.push(SuggestPage, { poi, availableTimes, orderedServicesIds, orderedServicesNames })
  }

  goBack () {
    this.navCtrl.pop();
  }
}
