import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Welcome } from '../welcome/welcome'

@Component({
  selector: 'billing-page',
  templateUrl: 'billing.html'
})
export class BillingPage {
  
  constructor(public navCtrl: NavController) {
  }

  goWelcome (params) {
    if (!params) params = {};
    this.navCtrl.push(Welcome);
  }

}
