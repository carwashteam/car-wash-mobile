import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { TabsControllerPage } from '../tabs-controller/tabs-controller';

@Component({
  selector: 'welcome-page',
  templateUrl: 'welcome.html'
})
export class Welcome {
  
  constructor(public navCtrl: NavController) {
  }

  goTabs (params) {
    if (!params) params = {};
    this.navCtrl.push(TabsControllerPage);
  }
  
}
