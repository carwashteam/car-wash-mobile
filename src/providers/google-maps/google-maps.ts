import { Injectable, ElementRef, OnDestroy } from '@angular/core';

@Injectable()
export class GoogleMapsProvider implements OnDestroy {
  static markerIcon: string = 'assets/img/marker32.png';
  static carIcon: string = 'assets/img/car32.png'
  infoWindow: any;
  private selectedMarker: google.maps.Marker;

  constructor() {
    console.log('Constructing GoogleMapsProvider Provider');
  }

  ngOnDestroy () {
   console.info('%cGoogleMaps Provider instance destroyed...', 'font-weight:600;color:red;') ;
  }

  initMap (element: ElementRef, center: google.maps.LatLng) {
    const mapOptions = {
      center: center,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    return new google.maps.Map(element.nativeElement, mapOptions);
  }

  /*addMarkerOnMap (location: google.maps.LatLng, map: google.maps.Map, content: string = '<h5>Car WASH!!!</h5>', animation?: boolean) {
    
    const marker = new google.maps.Marker({
      map: map,
      animation: animation ? google.maps.Animation.DROP : undefined,
      icon: GoogleMapsProvider.markerIcon,
      position: location
    });

    this.addNewInfoWindow(map, marker, content);
  }*/

  addCarMarkerOnMap (location: google.maps.LatLng, map: google.maps.Map, content?: string, animation?: boolean) {
    const marker = new google.maps.Marker({
      map: map,
      animation: animation ? google.maps.Animation.DROP : undefined,
      icon: GoogleMapsProvider.carIcon,
      position: location,
      zIndex: 4
    });

    if(content) this.addNewInfoWindow(map, marker, content);
  }

  addNewInfoWindow (map: google.maps.Map, marker: google.maps.Marker, content: string) { // Adding new dedicated InfoWindow to each marker...
    const infoWindow = new google.maps.InfoWindow({
      content: content
    });
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(map, marker);
    });
  }
  createInfoWindow (content?: string) {
   return new google.maps.InfoWindow({
      content: content
    }); 
  }
  setInfoWindowContent (infoWindow: google.maps.InfoWindow, content: string) {
    return infoWindow.setContent(content)
  }

  addInfoWindowToMarker (infoWindow: google.maps.InfoWindow, map: google.maps.Map, marker: google.maps.Marker, content?: string) {
    google.maps.event.addListener(marker, 'click', () => {
      if(content) this.setInfoWindowContent(infoWindow, content);
      // if (marker.getAnimation() !== null)
      marker.setAnimation(google.maps.Animation.BOUNCE);
      if((this.selectedMarker) && (this.selectedMarker !== marker)) this.selectedMarker.setAnimation(null);
      this.selectedMarker = marker;
      infoWindow.open(map, marker);
    });
  }

  // Adds a marker to the map and push to the array.
  createMarker(location, animation?: boolean) {
    return new google.maps.Marker({
      animation: animation ? google.maps.Animation.DROP : undefined,
      icon: GoogleMapsProvider.markerIcon,
      position: location,
      zIndex: 3
    });
    // markers.push(marker);
  }

  startNavigating (origin: google.maps.LatLng, destination: google.maps.LatLng, map: google.maps.Map, directionsPanel: ElementRef) {
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;

    directionsDisplay.setMap(map);
    directionsDisplay.setPanel(directionsPanel.nativeElement);

    directionsService.route({
      origin: origin,
      destination: destination,
      travelMode: google.maps.TravelMode['DRIVING'],
      drivingOptions: {
        departureTime: new Date(Date.now()),  // for the time N milliseconds from now.
        trafficModel: google.maps.TrafficModel.PESSIMISTIC
      }
    }, (directionsResult, status) => {
      if(status == google.maps.DirectionsStatus.OK){
          directionsDisplay.setDirections(directionsResult);
      } else {
          console.warn(status);
      }
    });
  }

  calculateDrivingTime (origin: google.maps.LatLng, destinations: google.maps.LatLng[], callback: (responce: any, status: any) => void) {
    const options = {
      origins: [origin],
      destinations: destinations,
      travelMode: google.maps.TravelMode['DRIVING'],
      drivingOptions: {
        departureTime: new Date(Date.now()),  // for the time N milliseconds from now.
        trafficModel: google.maps.TrafficModel.PESSIMISTIC
      }
    };
    
    const service = new google.maps.DistanceMatrixService();
    service.getDistanceMatrix(options, callback);
  }

  // Sets the map on all markers in the array.
  private setMapOnAll(map: google.maps.Map, markers: Array<google.maps.Marker>) {
    for (let i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  clearMarkers(markers: Array<google.maps.Marker>) {
    this.setMapOnAll(null, markers);
  }

  // Shows any markers currently in the array.
  showMarkers(map: google.maps.Map, markers: Array<google.maps.Marker>) {
    this.setMapOnAll(map, markers);
  }

  // Deletes all markers in the array by removing references to them.
  deleteMarkers(markers: Array<google.maps.Marker>) {
    this.clearMarkers(markers);
  }

}
