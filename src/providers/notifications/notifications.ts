import { Injectable } from '@angular/core';
import { LoadingController, Loading, ToastController } from 'ionic-angular';

@Injectable()
export class NotificationsProvider {
  private loader: Loading;

  constructor(
    private loadingCtrl: LoadingController,
    public toastCtrl: ToastController
  ) {
    console.log('Initiating Notifications Provider');
  }

  showLoader (content: string) {
    this.loader = this.loadingCtrl.create({
      content
    })
    this.loader.present();
  }
  // FIXME: test drive this approach.
  // It may happen that implementation should be changed in order to support multile loaders
  // In some multiple async cases loader could be dismised before the end of some async routines
  dismissLoader () {
    this.loader.dismiss();
  }

  presentToast(message: string) {
    const toast = this.toastCtrl.create({
      message,
      position: 'top',
      duration: 4000,
      cssClass: 'success'
    });
    toast.present();
  }
// cssClass
   showError(message: string) {
    const toast = this.toastCtrl.create({
      message,
      position: 'top',
      duration: 6000,
      showCloseButton: true,
      cssClass: 'error',
      // dismissOnPageChange: true
    });
    toast.present();
  }
}
