import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable'
import { Platform } from 'ionic-angular';
import { Device } from '@ionic-native/device';

import { AppSettings } from '../../app/app.settings'

const TIMEOUT = 120000; // 2 minutes

 // TODO: try to remove most of private methods (and constructor logic) to parent base class BaseService
@Injectable()
export class API {

  private ENDPOINT:string = '';

  constructor(private http: Http, private storage: Storage, private platform: Platform, private device: Device) {
    /* this.platform.ready()
      .then((readySource) => {
        console.info('%cPlatform ready: %o', 'color:#F08080;font-weight:700;' , readySource);
        this.setAPI(); 
      });
    */
    this.ENDPOINT = this.platform.is('cordova') ? AppSettings.API_ENDPOINT_PROD : AppSettings.API_ENDPOINT_DEV
  
    this.getToken()
      .subscribe(({token}) => {
        console.info('%cToken: %o', 'color:#F08080;font-weight:700;' , token);
        this.saveToken(token);
    });
  }

  private getData (url: string) {
    url = (url.charAt(0) === `/`) ? url : `/${url}` // if one forgot the slash
    return this.http.get(`${this.ENDPOINT}${url}`)
      .map(this.extractJSON)
      .timeout(TIMEOUT)
      .do(this.logResponse)
      .catch(this.catchError);
  }

  private async postData (url: string, data: any, options?: RequestOptionsArgs) {
    url = (url.charAt(0) === `/`) ? url : `/${url}` // if one forgot the slash
    const _token = await this.loadToken();
    return this.http.post(`${this.ENDPOINT}${url}`, {...data, _token}, options)
      .map(this.extractJSON)
      .timeout(TIMEOUT)
      .do(this.logResponse)
      .catch(this.catchError)
      .toPromise();
  }

  private catchError (error: Response | any) {
    console.error('HTTP ERROR: ', error);
    // return Observable.throw(error.json().error || `Server Error`)
    return Observable.of(error.json());
  }

  private logResponse (res: Response) {
    console.info('%cAPI response: %o', 'color:orange;font-weight:700;', res)
  }

  private extractJSON (res: Response) {
    return res.json()
  }

  // private setAPI (readySource?: string) {
  //   this.ENDPOINT = readySource === 'dom' 
  //     ? AppSettings.API_ENDPOINT_DEV
  //     : AppSettings.API_ENDPOINT_PROD
  //     console.info('%cENDPOINT set to: %o', 'color:red;font-weight:700;', this.ENDPOINT)
  // }

  private saveToken (token: string) {
    return this.storage.set('_token', token)
  }

  private async loadToken () {
    return await this.storage.get('_token');
  }

  private async hasTelephone () {
    return await this.storage.get('tel');
  }

  private async hasPIN () {
    return await this.storage.get('pin');
  }

  private saveTelephone (tel: string) {
    return this.storage.set('tel', tel);
  }

  private savePIN (pin: string) {
    return this.storage.set('pin', pin)
  }

  private getUUID () {
    return this.device.uuid || AppSettings.DEV_UUID;
  }
  
  // TODO: Add documentation to all public methods
  async isAuthenticated () {
    const hasTelephone: boolean = await this.hasTelephone()
    const hasPIN: boolean = await this.hasPIN()
    return (hasTelephone && hasPIN)
  }
  getToken () { 
    return this.getData('/token');
  }
  getList () {
    return this.getData('/poi');
  }

  getOrders () {
    return this.getData('/orders');
  }

  async checkIfDone () {
    const { success } = await this.postData('/order/check', {});
    return !!success;
  }

  async createOrder (poi_id: string, price_id: string[], time: string) {
    const order = await this.postData('/order/new', {
      poi_id, price_id, time
    });
    return order;
  }

  // POST '/signup'. Параметры: {tel,uid,_token}
  async signUp (tel: string) {
    const uid = this.getUUID();
    const { success } = await this.postData('/signup', {
      tel, uid
    });

    if (success) this.saveTelephone(tel);
    
    return !!success;
  }

  // POST '/sms'. Параметры: {tel,uid,sms,_token}
  async confirmSMS (sms: string) {
    const uid = this.getUUID();
    // const _token = await this.loadToken();
    const tel = await this.hasTelephone();
    if(!tel) return;
    const { success } = await this.postData('/sms', {
      tel, uid, sms
    });
    
    if (success) {
      this.savePIN(sms);
    }
    return !!success;
  }

// should return true if LoggedIn (pinging server every time), if not - false
// first we have to check the storage.
// If tel and pin code are available - we can login
// if some of this info is not available - please signup 
  async signIn () {
    const isAuthenticated: boolean = await this.isAuthenticated();
    if (!isAuthenticated) return false;

    const uid: string = this.device.uuid || AppSettings.DEV_UUID;
    const tel: string = await this.hasTelephone();
    const { success } = await this.postData('/signin', {
      tel, uid
    });
    
    console.info('%cSIGNED IN: %o', 'color:green; font-style:italic;font-weight:700;' , success);
    return !!success;
  }
}
